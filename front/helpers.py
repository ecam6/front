from django.shortcuts import render
from django.conf import settings
from ptl_api import new_client, Configuration
from swagger_codegen.api.adapter.requests import RequestsAdapter

def global_render(request, template, params = {}):
    client = new_client(RequestsAdapter(), Configuration(host=settings.API_URL))
    params["sidebar_workstations"] = client.workstations.workstations_list()
    return render(request, template, params)
