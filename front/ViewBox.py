from django.shortcuts import redirect
from django.conf import settings

from ptl_api import new_client, Configuration
from swagger_codegen.api.adapter.requests import RequestsAdapter


def add(request, url=None):
    if request.method == "POST":
        client = new_client(RequestsAdapter(), Configuration(host=settings.API_URL))
        data = {
            "pos": request.POST["pos"],
            "size": request.POST["size"],
            "line": request.POST["line"],
            "article": request.POST["article"],
        }
        client.boxes.boxes_create(data)

    return redirect(request.META["HTTP_REFERER"])


def delete(request, id, url=None):
    client = new_client(RequestsAdapter(), Configuration(host=settings.API_URL))
    try:
        client.boxes.boxes_destroy(id)
    except KeyError as e:
        pass
    return redirect(request.META["HTTP_REFERER"])
