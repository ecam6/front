# from django.shortcuts import render
from django.views.generic import ListView
from front.helpers import global_render
from django.conf import settings

from ptl_api import new_client, Configuration
from swagger_codegen.api.adapter.requests import RequestsAdapter


def find_workstation(workstations, pos):
    search = [w for w in workstations.results if w.pos == pos]
    if not search:
        return None
    return search[0]


# Create your views here.
def dashboard(request, url=None):
    client = new_client(RequestsAdapter(), Configuration(host=settings.API_URL))
    Workstations = client.workstations.workstations_list()
    ProductionLine = [
        [None, find_workstation(Workstations, 4), None],
        [find_workstation(Workstations, 5), None, find_workstation(Workstations, 3)],
        [find_workstation(Workstations, 6), None, find_workstation(Workstations, 2)],
        [find_workstation(Workstations, 7), None, find_workstation(Workstations, 1)],
    ]

    return global_render(
        request, "dashboard/index.html", {"ProductionLine": ProductionLine}
    )


def contact(request, url=None):
    return global_render(request, "contact.html")
