from django.shortcuts import render, redirect
from django.conf import settings

from front.helpers import global_render
from django.urls import reverse

from ptl_api import new_client, Configuration
from swagger_codegen.api.adapter.requests import RequestsAdapter


def getset_setting(client, str_setting, posted_value):
    setting = client.settings.settings_retrieve(str_setting)
    if posted_value != None:
        setting = client.settings.settings_partial_update(
            {"value": posted_value}, setting.key
        )
    return setting.value


def list(request, url=None):
    client = new_client(RequestsAdapter(), Configuration(host=settings.API_URL))
    Workstations = client.workstations.workstations_list()

    led_color = getset_setting(client, "LED_COLOR", request.POST.get("ledcolor", None))
    led_brightness = getset_setting(
        client, "LED_BRIGHTNESS", request.POST.get("ledbrightness", None)
    )
    mode = getset_setting(client, "MODE", request.POST.get("mode", None))

    return global_render(
        request,
        "workstations/list.html",
        {
            "page_title": "All Workstations",
            "Workstations": Workstations,
            "led_color": led_color,
            "led_brightness": led_brightness,
            "mode": mode,
        },
    )


def reset(request, url=None):
    client = new_client(RequestsAdapter(), Configuration(host=settings.API_URL))
    boxes = client.boxes.boxes_list()
    for box in boxes.results:
        client.boxes.boxes_partial_update({"is_on": False}, box.id)
    return redirect(request.META["HTTP_REFERER"])


def sync(request, url=None):
    client = new_client(RequestsAdapter(), Configuration(host=settings.API_URL))
    client.workstations.workstations_sync_retrieve()
    client.articles.articles_sync_retrieve()
    return redirect(request.META["HTTP_REFERER"])


def retrieve(request, id, url=None):
    client = new_client(RequestsAdapter(), Configuration(host=settings.API_URL))
    workstation = client.workstations.workstations_retrieve(id)
    return global_render(
        request,
        "workstations/retrieve.html",
        {
            "page_title": "Poste " + str(workstation.pos),
            "Workstation": workstation,
            "editable_breadcrumb_link": reverse(
                "front.workstation.edit", kwargs={"id": workstation.id}
            ),
        },
    )


def add(request, url=None):
    if request.method == "POST":
        client = new_client(RequestsAdapter(), Configuration(host=settings.API_URL))
        w_create = client.workstations.workstations_create({"pos": request.POST["pos"]})
        workstation = [
            w
            for w in client.workstations.workstations_list().results
            if w.pos == w_create.pos
        ][0]
        for i in range(1, int(request.POST["nb_line"]) + 1):
            client.lines.lines_create({"pos": i, "workstation": workstation.id})

    return global_render(
        request, "workstations/add.html", {"page_title": "Create a Workstation"}
    )


def edit(request, id, url=None):
    client = new_client(RequestsAdapter(), Configuration(host=settings.API_URL))
    workstation = client.workstations.workstations_retrieve(id)
    articles = client.articles.articles_list()
    iolinks = client.iolinks.iolinks_list()
    if request.method == "POST":
        data = {"pos": request.POST["pos"]}
        if "iolink" in request.POST:
            data["iolink"] = request.POST["iolink"]
            if request.POST["iolink"]:
                data["iolink_port"] = request.POST["iolink_port"]
        workstation = client.workstations.workstations_partial_update(data, id)

    return global_render(
        request,
        "workstations/edit.html",
        {
            "page_title": "Edit Workstation " + str(workstation.pos),
            "Workstation": workstation,
            "Size": {"B": "Big", "S": "Small"},
            "Articles": articles,
            "Iolinks": iolinks,
        },
    )


def delete(request, id, url=None):
    client = new_client(RequestsAdapter(), Configuration(host=settings.API_URL))
    try:
        client.workstations.workstations_destroy(id)
    except KeyError as e:
        pass
    return redirect(request.META["HTTP_REFERER"])
