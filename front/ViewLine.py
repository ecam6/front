from django.shortcuts import redirect
from django.conf import settings

from ptl_api import new_client, Configuration
from swagger_codegen.api.adapter.requests import RequestsAdapter


def add(request, url=None):
    if request.method == "POST":
        client = new_client(RequestsAdapter(), Configuration(host=settings.API_URL))
        client.lines.lines_create(
            {"pos": request.POST["pos"], "workstation": request.POST["workstation"]}
        )

    return redirect(request.META["HTTP_REFERER"])


def delete(request, id, url=None):
    client = new_client(RequestsAdapter(), Configuration(host=settings.API_URL))
    try:
        client.lines.lines_destroy(id)
    except KeyError as e:
        pass
    return redirect(request.META["HTTP_REFERER"])
