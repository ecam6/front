from django.urls import path
from django.conf.urls import url
from front import views, ViewWorkstation, ViewBox, ViewLine

urlpatterns = [
    path("", views.dashboard, name="front.dashboard"),
    path("contact", views.contact, name="front.contact"),
    # Workstation
    path("settings/workstations", ViewWorkstation.list, name="front.workstation.list"),
    path(
        "settings/workstations/reset",
        ViewWorkstation.reset,
        name="front.workstation.reset",
    ),
    path(
        "settings/workstations/sync",
        ViewWorkstation.sync,
        name="front.workstation.sync",
    ),
    path(
        "settings/workstations/add", ViewWorkstation.add, name="front.workstation.add"
    ),
    path(
        "settings/workstations/<uuid:id>/",
        ViewWorkstation.retrieve,
        name="front.workstation.retrieve",
    ),
    path(
        "settings/workstations/<uuid:id>/edit",
        ViewWorkstation.edit,
        name="front.workstation.edit",
    ),
    path(
        "settings/workstations/<uuid:id>/delete",
        ViewWorkstation.delete,
        name="front.workstation.delete",
    ),
    path("settings/lines/add", ViewLine.add, name="front.line.add"),
    path("settings/lines/<uuid:id>/delete", ViewLine.delete, name="front.line.delete"),
    path("settings/boxes/add", ViewBox.add, name="front.box.add"),
    path("settings/boxes/<uuid:id>/delete", ViewBox.delete, name="front.box.delete"),
]
