from __future__ import annotations

from swagger_codegen.api.base import BaseApi

from . import workstations_list
from . import workstations_create
from . import workstations_retrieve
from . import workstations_update
from . import workstations_partial_update
from . import workstations_destroy
from . import workstations_sync_retrieve
class WorkstationsApi(BaseApi):
    workstations_list = workstations_list.make_request
    workstations_create = workstations_create.make_request
    workstations_retrieve = workstations_retrieve.make_request
    workstations_update = workstations_update.make_request
    workstations_partial_update = workstations_partial_update.make_request
    workstations_destroy = workstations_destroy.make_request
    workstations_sync_retrieve = workstations_sync_retrieve.make_request