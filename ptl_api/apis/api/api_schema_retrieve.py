from __future__ import annotations

import datetime
import pydantic
import typing

from pydantic import BaseModel

from swagger_codegen.api.base import BaseApi
from swagger_codegen.api.request import ApiRequest
from swagger_codegen.api import json
def make_request(self: BaseApi,


    format: str = ...,

    lang: str = ...,

) -> typing.Dict:
    

    
    body = None
    

    m = ApiRequest(
        method="GET",
        path="/api/schema/".format(
            
        ),
        content_type=None,
        body=body,
        headers=self._only_provided({
        }),
        query_params=self._only_provided({
                "format": format,
            
                "lang": lang,
            
        }),
        cookies=self._only_provided({
        }),
    )
    return self.make_request({
    
        "200": {
            
                "application/json": typing.Dict,
            
                "application/vnd.oai.openapi": typing.Dict,
            
                "application/vnd.oai.openapi+json": typing.Dict,
            
                "application/yaml": typing.Dict,
            
        },
    
    }, m)