from __future__ import annotations

import datetime
import pydantic
import typing

from pydantic import BaseModel

from swagger_codegen.api.base import BaseApi
from swagger_codegen.api.request import ApiRequest
from swagger_codegen.api import json
class PatchedEvent(BaseModel):
    id_article_ext: typing.Optional[str]  = None
    id_ext: typing.Optional[str]  = None
    id_workstation_ext: typing.Optional[str]  = None

class Event(BaseModel):
    id: str 
    id_article_ext: str 
    id_ext: str 
    id_workstation_ext: str 

def make_request(self: BaseApi,

    __request__: PatchedEvent,


    id: str,

) -> Event:
    

    
    body = __request__
    

    m = ApiRequest(
        method="PATCH",
        path="/events/{id}/".format(
            
                id=id,
            
        ),
        content_type="application/json",
        body=body,
        headers=self._only_provided({
        }),
        query_params=self._only_provided({
        }),
        cookies=self._only_provided({
        }),
    )
    return self.make_request({
    
        "200": {
            
                "application/json": Event,
            
        },
    
    }, m)