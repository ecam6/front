from __future__ import annotations

import datetime
import pydantic
import typing

from pydantic import BaseModel

from swagger_codegen.api.base import BaseApi
from swagger_codegen.api.request import ApiRequest
from swagger_codegen.api import json
class Setting(BaseModel):
    key: str 
    value: str 

class PaginatedSettingList(BaseModel):
    count: typing.Optional[int]  = None
    next: typing.Optional[typing.Union[str, None]]  = None
    previous: typing.Optional[typing.Union[str, None]]  = None
    results: typing.Optional[typing.List[Setting]]  = None

def make_request(self: BaseApi,


    page: int = ...,

) -> PaginatedSettingList:
    

    
    body = None
    

    m = ApiRequest(
        method="GET",
        path="/settings/".format(
            
        ),
        content_type=None,
        body=body,
        headers=self._only_provided({
        }),
        query_params=self._only_provided({
                "page": page,
            
        }),
        cookies=self._only_provided({
        }),
    )
    return self.make_request({
    
        "200": {
            
                "application/json": PaginatedSettingList,
            
        },
    
    }, m)