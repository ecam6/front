from __future__ import annotations

import datetime
import pydantic
import typing

from pydantic import BaseModel

from swagger_codegen.api.base import BaseApi
from swagger_codegen.api.request import ApiRequest
from swagger_codegen.api import json
class PatchedSetting(BaseModel):
    key: typing.Optional[str]  = None
    value: typing.Optional[str]  = None

class Setting(BaseModel):
    key: str 
    value: str 

def make_request(self: BaseApi,

    __request__: PatchedSetting,


    key: str,

) -> Setting:
    

    
    body = __request__
    

    m = ApiRequest(
        method="PATCH",
        path="/settings/{key}/".format(
            
                key=key,
            
        ),
        content_type="application/json",
        body=body,
        headers=self._only_provided({
        }),
        query_params=self._only_provided({
        }),
        cookies=self._only_provided({
        }),
    )
    return self.make_request({
    
        "200": {
            
                "application/json": Setting,
            
        },
    
    }, m)