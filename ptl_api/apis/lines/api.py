from __future__ import annotations

from swagger_codegen.api.base import BaseApi

from . import lines_list
from . import lines_create
from . import lines_retrieve
from . import lines_update
from . import lines_partial_update
from . import lines_destroy
class LinesApi(BaseApi):
    lines_list = lines_list.make_request
    lines_create = lines_create.make_request
    lines_retrieve = lines_retrieve.make_request
    lines_update = lines_update.make_request
    lines_partial_update = lines_partial_update.make_request
    lines_destroy = lines_destroy.make_request