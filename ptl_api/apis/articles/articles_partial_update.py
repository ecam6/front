from __future__ import annotations

import datetime
import pydantic
import typing

from pydantic import BaseModel

from swagger_codegen.api.base import BaseApi
from swagger_codegen.api.request import ApiRequest
from swagger_codegen.api import json
class PatchedBox(BaseModel):
    article: typing.Optional[typing.Union[str, None]]  = None
    is_on: typing.Optional[bool]  = None
    line: typing.Optional[str]  = None
    pos: typing.Optional[int]  = None
    size: typing.Optional[str]  = None

class PatchedArticle(BaseModel):
    boxes: typing.Optional[typing.List[PatchedBox]]  = None
    id_ext: typing.Optional[str]  = None
    name: typing.Optional[str]  = None

class Box(BaseModel):
    article: typing.Optional[typing.Union[str, None]]  = None
    article_name: str 
    col_size: int 
    id: str 
    is_on: typing.Optional[bool]  = None
    line: str 
    pos: int 
    size: str 

class Article(BaseModel):
    boxes: typing.Optional[typing.List[Box]]  = None
    id: str 
    id_ext: str 
    name: str 

def make_request(self: BaseApi,

    __request__: PatchedArticle,


    id: str,

) -> Article:
    

    
    body = __request__
    

    m = ApiRequest(
        method="PATCH",
        path="/articles/{id}/".format(
            
                id=id,
            
        ),
        content_type="application/json",
        body=body,
        headers=self._only_provided({
        }),
        query_params=self._only_provided({
        }),
        cookies=self._only_provided({
        }),
    )
    return self.make_request({
    
        "200": {
            
                "application/json": Article,
            
        },
    
    }, m)