build:
	docker build -t ecam6/plt/front:dev .
dev_env:
	docker run -it --rm -v $(shell pwd):/app ecam6/plt/front:dev sh
migrations:
	docker run -it --rm -v $(shell pwd):/app ecam6/plt/front:dev python3 manage.py makemigrations
migrate:
	docker run -it --rm -v $(shell pwd):/app ecam6/plt/front:dev python3 manage.py migrate
seed:
	docker run -it --rm -v $(shell pwd):/app ecam6/plt/front:dev python3 manage.py loaddata deploy.json
check:
	docker run -it --rm -v $(shell pwd):/app ecam6/plt/front:dev python3 manage.py check
dump:
	docker run -it --rm -v $(shell pwd):/app ecam6/plt/front:dev python3 manage.py dumpdata > db.json
run:
	docker run -it --rm -v $(shell pwd):/app -p 8002:8000 ecam6/plt/front:dev python3 manage.py runserver 0.0.0.0:8000
vscode:
	mkdir .vscode && echo '{"python.pythonPath": "/usr/bin/python3","python.envFile": "$${workspaceFolder}/.vscodeenv"}' > ./.vscode/settings.json && echo 'PYTHONPATH=./ptl_app:$${PYTHONPATH}' > .vscodeenv
swag_gen:
	docker run -it --rm -v $(shell pwd):/app ecam6/plt/front:dev swagger_codegen generate http://10.20.0.16:8003/api/schema ptl_api
