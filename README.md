# API Pick to light

Ce projet appartient au projet [pick to light](https://gitlab.com/ecam6/ptl_apps) et se base sur [Django](https://www.djangoproject.com/).

Web interface permettant de configuration facilement l'application, de voir l'état en temps réel des leds sur la ligne de production.

# Documentation

Respecte les principes de Django.

Cette applications ne possède pas de modèles puisqu'elle fait appel à l'API.

La partie temps réel est uniquement faite en Javascript et communique directement avec le MQTT.

La logique se trouve dans les fichiers `front/View*.py` et `front/views.py`

Le code HTML dans le dossier `front/templates`

# Variable d'environement

- DJANGO_SECRET_KEY: Must be unique
- API_HOST: (default api)
- API_PORT: (default 8000)
- DJANGO_DEBUG: True/False

# Déploiement, usage, configuration...

Ne peut pas être utilisé seul, se référé au [projet principal](https://gitlab.com/ecam6/ptl_apps)
