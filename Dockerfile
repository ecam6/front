FROM python:3.8.7
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
WORKDIR /app

COPY requirements.txt ./
RUN pip3 install --no-cache-dir -r requirements.txt

COPY . .

EXPOSE 8000

CMD ["python3", "manage.py" ,"runserver", "0.0.0.0:8000"]
# CMD ["gunicorn", "--bind", ":8000", "--workers", "3", "ptl_app.wsgi:application"]